import { writable } from 'svelte/store';
import { ethers } from 'ethers';
import {
  anonymiceAbi,
  breedingAbi,
  cheethAbi,
  cheethv2Abi,
  openseaAbi,
} from './abi.js';

/** Contract Interfaces */
const anonymiceIface = new ethers.utils.Interface(anonymiceAbi());
const breedingIface = new ethers.utils.Interface(breedingAbi());
const cheethIface = new ethers.utils.Interface(cheethAbi());
const cheethv2Iface = new ethers.utils.Interface(cheethv2Abi());
const openseaIface = new ethers.utils.Interface(openseaAbi());
export const IAnonymice = writable(anonymiceIface);
export const IBreeding = writable(breedingIface);
export const ICheeth = writable(cheethIface);
export const ICheethv2 = writable(cheethv2Iface);
export const IOpenSea = writable(openseaIface);

/** Ethers.js stores */
export const provider = writable(null);
export const signer = writable(null);
export const currentBlock = writable(0);

/** Contains the result of provider.getNetwork() */
export const network = writable({});

/** Eth address and balances */
export const walletConnected = writable(false);
export const address = writable(null);
export const addressToLookup = writable(null);
export const ethBalance = writable(0);
export const cheethBalance = writable(0);
export const cheethv2Balance = writable(0);
export const unclaimedCheethBalance = writable(0);
export const unclaimedCheethv2Balance = writable(0);
export const cheethTransfers = writable([]);

/** Contract addresses */
export const anonymiceAddress = writable('0xbad6186E92002E312078b5a1dAfd5ddf63d3f731');
export const cheethAddress = writable('0x5f7BA84c7984Aa5ef329B66E313498F0aEd6d23A');
export const cheethv2Address = writable('0x54C4419b7be48889097a70Ef6Bdc47feAC54AEF5');
export const cheethv3Address = writable('0xeBd707E7b27c6c14C98a327e332a0DF7C6C015b0');
export const breedingAddress = writable('0x15Cc16BfE6fAC624247490AA29B6D632Be549F00');
export const openseaAddress = writable('0x7Be8076f4EA4A4AD08075C2508e481d6C946D12b');
export const burnAddress = writable('0x000000000000000000000000000000000000dead');

/** Contract creation block.  Used for queryFilter() */
export const anonymiceBlock = writable(13238868);

/** Contract instances */
export const anonymiceContact = writable(null);
export const cheethContact = writable(null);
export const cheethv2Contact = writable(null);
export const cheethv3Contact = writable(null);
export const breedingContract = writable(null);

/** Arrays of mouse IDs */
export const miceInWallet = writable([]);
export const miceStaked = writable([]);
export const miceStakedv2 = writable([]);
export const miceBabies = writable([]);
export const miceLocked = writable([]);

/** Map baby mouse ID to it's incubator info */
export const babyIncubators = writable(new Map());

/** Map mouse ID to it's metadata */
export const miceMetadata = writable(new Map());
export const babyMiceMetadata = writable(new Map());

/** Mouse ID selected to display in MouseDetail */
export const selectedMouse = writable(null);

/** Baby and genesis mouse ids for lookup */
export const genesisMouseID = writable('');
export const babyMouseID = writable('');

/** 'baby', 'genesis' */
export const selectedMouseType = writable('');

/** Boolean.  Has user approved cheeth contract to use mice? */
export const miceApprovedForStaking = writable();

/** Boolean.  When set to true, overflow is hidden on html root element */
export const modal = writable();
