import { ethers } from 'ethers';

/**
 * Convert wei to ETH with four decimals for display. e.g. '1.4269'.
 * @param {BigNumber} wei - The result from provider.getBalance().
 * @returns {String} balance - A string to display the account balance in ETH.
 */
export function formatBalance(wei) {
  if (!wei) {
    return;
  }
  const remainder = wei.mod(1e14);
  const balance = ethers.utils.formatEther(wei.sub(remainder));
  return `${balance}`;
}

/**
 * Format an ETH address for display. e.g. 0x0000...dead
 * @param {String} address - Ethereum address.
 * @returns {String} formattedAddress - Fist six and last four digits of address.
 */
export function formatAddress(address) {
  const firstSix = address.slice(0, 6);
  const lastFour = address.slice(38, 42);
  return `${firstSix}...${lastFour}`;
}

/**
 * Exponetial backoff delay for retrying contract calls.
 * @param {Number} retryCount - The current retry count.
 * @returns {Promise} - Resolves after timeout. 
 */
function wait(retryCount) {
  return new Promise(resolve => setTimeout(resolve, 10 ** retryCount));
}

/**
 * Retry a failed contract call
 * Sometimes contract calls return "{code: -32000, message: 'header not found'}".
 * This only seems to happen when metamask is using Infura api.
 * Using a local node doesn't produce this error.
 * @param {Function} fn - The function to retry.
 * @param {Array} args - An array of arguments for the fuction
 * @param {Number} retries - Number of times to retry
 * @returns The result of function.
 */
export async function retry(fn, args=[], retries=5) {
  for (let i = 0; i < retries; i++) {
    try {
      await wait(i);
      return await fn(...args);
    } catch (e) {
      console.log(e);
      console.log(`attemp ${i} failed.  retry...`, fn)
    }
  }

  throw new Error(`Failed retrying ${retries} times`);
}

  /** Convert result from contract.tokenURI() into JSON */
  export function parseDataUriResult(jsonDataUri) {
    // 29 = length of "data:application/json;base64,"
    const json = window.atob(jsonDataUri.substring(29));
    const tokenMetadata = JSON.parse(json);
    return tokenMetadata;
  };
