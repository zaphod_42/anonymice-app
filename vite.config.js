import { defineConfig } from 'vite';
import { svelte } from '@sveltejs/vite-plugin-svelte';

// https://vitejs.dev/config/
export default defineConfig({
  build: {
  	target: 'modules',
  	outDir: 'dist',
  	cssCodeSplit: true
  },
  plugins: [svelte({
    compilerOptions: {
      dev: process.env.NODE_ENV === 'development'
    }
  })]
})
