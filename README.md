## Get started

Install the dependencies...

```bash
cd anonymice_app
npm install
```

...then start [Vite](https://vitejs.dev):

```bash
npm run dev
```

## Recommended IDE Setup

[VS Code](https://code.visualstudio.com/) + [Svelte](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode).

## Building and running in production mode

To create an optimised production ready version of the app:

```bash
npm run build
```
